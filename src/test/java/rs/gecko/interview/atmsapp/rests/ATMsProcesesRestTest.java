package rs.gecko.interview.atmsapp.rests;

import java.util.List;
import org.junit.After;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import rs.gecko.interview.atmsapp.BeansLister;
import rs.gecko.interview.atmsapp.TestUtils;
import rs.gecko.interview.atmsapp.entities.ATM;
import rs.gecko.interview.atmsapp.services.ATMsService;
import rs.gecko.interview.atmsapp.services.TranslatableEnumerator;

/**
 *
 * @author dragank
 */
@RunWith( SpringJUnit4ClassRunner.class )
@ContextConfiguration(
    loader = AnnotationConfigContextLoader.class,
    classes = {
        BeansLister.class
    }
)
public class ATMsProcesesRestTest {

    private IATMsRest iATMsRest;
    @Autowired
    public void setiATMsRest(IATMsRest iATMsRest) {
        this.iATMsRest = iATMsRest;
    }
    @Autowired
    private IATMsProcesesRest iATMsProcesesRest;
    public void setiATMsProcesesRest(IATMsProcesesRest iATMsProcesesRest) {
        this.iATMsProcesesRest = iATMsProcesesRest;
    }
    
    @After
    public void dbCleaup() {
        System.out.println("dbCleaup - ATM");
        iATMsProcesesRest.deleteAll();
    }
    
    @Test
    public void testDeleteAll() {
        System.out.println("deleteAll");
        ATM input = TestUtils.createOneATM();
        IATMsRest instance = iATMsRest;
        TranslatableEnumerator result = instance.put(input);
        input.getAddress().setCity("ha" + input.getAddress().getCity());
        result = instance.post(input);
        input = TestUtils.createOneATM();
        input.getAddress().setCity("hb" + input.getAddress().getCity());
        result = instance.post(input);
        input = TestUtils.createOneATM();
        input.getAddress().setCity("hc" + input.getAddress().getCity());
        result = instance.post(input);
        input = TestUtils.createOneATM();
        input.getAddress().setCity("hd" + input.getAddress().getCity());
        result = instance.post(input);
        input = TestUtils.createOneATM();
        input.getAddress().setCity("he" + input.getAddress().getCity());
        result = instance.post(input);
        List<ATM> citiesByName = instance.listByCityName("H");
        int count5 = citiesByName.size();
        assertEquals(5, count5);
        TranslatableEnumerator response = iATMsProcesesRest.deleteAll();
        assertEquals(ATMsService.ATMServiceMsg.SUCCESS, response);
        assertEquals(0, instance.listByCityName("ha").size());
    }

    @Test
    public void testInit() {
        System.out.println("init");
        TranslatableEnumerator result = iATMsProcesesRest.init();
        assertEquals(ATMsService.ATMServiceMsg.SUCCESS, result);
    }
    
}
