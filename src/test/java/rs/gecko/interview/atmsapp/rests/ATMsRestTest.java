package rs.gecko.interview.atmsapp.rests;

import java.util.List;
import org.junit.After;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import rs.gecko.interview.atmsapp.BeansLister;
import rs.gecko.interview.atmsapp.TestUtils;
import rs.gecko.interview.atmsapp.entities.ATM;
import rs.gecko.interview.atmsapp.services.ATMsService;
import rs.gecko.interview.atmsapp.services.TranslatableEnumerator;

/**
 *
 * @author dragank
 */
@RunWith( SpringJUnit4ClassRunner.class )
@ContextConfiguration(
    loader = AnnotationConfigContextLoader.class,
    classes = {
        BeansLister.class
    }
)
public class ATMsRestTest {

@Autowired
    private IATMsRest iATMsRest;

    @Autowired
    private IATMsProcesesRest iATMsProcesesRest;

    @After
    public void dbCleaup() {
        System.out.println("dbCleaup - ATM");
        iATMsProcesesRest.deleteAll();
    }
    
    @Test
    public void testPost() {
        System.out.println("post");
        ATM input = TestUtils.createOneATM();
        IATMsRest instance = iATMsRest;
        TranslatableEnumerator expResult = ATMsService.ATMServiceMsg.SUCCESS;
        TranslatableEnumerator result = instance.post(input);
        assertEquals(expResult, result);
        input.setType(null);
        expResult = ATMsService.ATMServiceMsg.FAILURE;
        result = instance.post(input);
        assertEquals(expResult, result);
    }

    @Test
    public void testGet() {
        System.out.println("get");
        Long id = 1L;
        ATM input = TestUtils.createOneATM();
        IATMsRest instance = iATMsRest;
        instance.post(input);
        ATM expResult = null;
        ATM result = instance.get(id);
        assertEquals(expResult, result);
    }

    @Test
    public void testListByCityName() {
        System.out.println("listByCityName");
        String cityName = "ha";
        ATM input = TestUtils.createOneATM();
        IATMsRest instance = iATMsRest;
        instance.post(input);
        List<ATM> result = instance.listByCityName(cityName);
        assertTrue(result.size() > 0);
    }

    @Test
    public void testCountByCityName() {
        System.out.println("countByCityName");
        String cityName = "ha";
        ATM input = TestUtils.createOneATM();
        IATMsRest instance = iATMsRest;
        instance.post(input);
        long count = instance.countByCityName(cityName);
        assertTrue(count > 0);
    }

    @Test
    public void testPut() {
        System.out.println("put");
        ATM input = TestUtils.createOneATM();
        IATMsRest instance = iATMsRest;
        TranslatableEnumerator expResult = ATMsService.ATMServiceMsg.SUCCESS;
        TranslatableEnumerator result = instance.put(input);
        assertEquals(expResult, result);
        input.setType(null);
        expResult = ATMsService.ATMServiceMsg.FAILURE;
        result = instance.post(input);
        assertEquals(expResult, result);
    }

    @Test
    public void testDelete() {
        System.out.println("delete");
        long id = 0L;
        long count = 0L;
       
        ATM input = TestUtils.createOneATM();
        IATMsRest instance = iATMsRest;
        TranslatableEnumerator result = instance.put(input);
        if(ATMsService.ATMServiceMsg.SUCCESS == result) {
            List<ATM> citiesByName = instance.listByCityName("ha");
            ATM atm = citiesByName.get(0);
            count = citiesByName.size();
            id = atm.getId();
            instance.delete(id);
            citiesByName = instance.listByCityName("ha");
            assert(citiesByName.size() + 1 == count);
        }
        else {
            fail("The test case is a prototype.");
        }
    }

    @Test
    public void testDeleteAll() {
        System.out.println("deleteAll");
        ATM input = TestUtils.createOneATM();
        IATMsRest instance = iATMsRest;
        TranslatableEnumerator result = instance.put(input);
        input.getAddress().setCity("ha" + input.getAddress().getCity());
        result = instance.post(input);
        input = TestUtils.createOneATM();
        input.getAddress().setCity("hb" + input.getAddress().getCity());
        result = instance.post(input);
        input = TestUtils.createOneATM();
        input.getAddress().setCity("hc" + input.getAddress().getCity());
        result = instance.post(input);
        input = TestUtils.createOneATM();
        input.getAddress().setCity("hd" + input.getAddress().getCity());
        result = instance.post(input);
        input = TestUtils.createOneATM();
        input.getAddress().setCity("he" + input.getAddress().getCity());
        result = instance.post(input);
        List<ATM> citiesByName = instance.listByCityName("H");
        int count5 = citiesByName.size();
        assertEquals(5, count5);
        TranslatableEnumerator response = iATMsProcesesRest.deleteAll();
        assertEquals(ATMsService.ATMServiceMsg.SUCCESS, response);
        assertEquals(0, instance.listByCityName("ha").size());
    }

    @Test
    public void testInit() {
        System.out.println("init");
        IATMsRest instance = iATMsRest;
        TranslatableEnumerator result = iATMsProcesesRest.init();
        assertEquals(ATMsService.ATMServiceMsg.SUCCESS, result);
    }

    @Test
    public void testListCityNames() {
        System.out.println("listCityNames");
        IATMsRest instance = iATMsRest;
        String cityName = "A";
        TranslatableEnumerator result = iATMsProcesesRest.init();
        assertEquals(ATMsService.ATMServiceMsg.SUCCESS, result);
        List<String> citiesByName = instance.listCityNames("ha");
        assert(citiesByName.size() > 0);
    }
    
}
