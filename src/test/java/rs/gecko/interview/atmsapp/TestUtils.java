package rs.gecko.interview.atmsapp;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import rs.gecko.interview.atmsapp.entities.ATM;

/**
 *
 * @author dragank
 */
public class TestUtils {

    public static final ATM createOneATM() {
        String atmJSON = "{\"address\":{\"street\":\"Zuiderhaven\",\"housenumber\":\"8\",\"postalcode\":\"8861 XB\",\"city\":\"Harlingen\",\"geoLocation\":{\"lat\":\"53.174016\",\"lng\":\"5.41296\"}},\"distance\":0,\"type\":\"ING\"}";
        ObjectMapper mapper = new ObjectMapper();
        ATM result = null;
        try {
            result = mapper.readerFor(ATM.class).readValue(atmJSON);
        } catch (IOException e) {
            e.printStackTrace(System.err);
        }
        return result;
    }
    
}
