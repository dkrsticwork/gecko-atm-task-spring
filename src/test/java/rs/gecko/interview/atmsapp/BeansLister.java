package rs.gecko.interview.atmsapp;

import org.eclipse.persistence.config.PersistenceUnitProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.persistence.spi.PersistenceUnitTransactionType;
import java.util.HashMap;
import java.util.Map;
import org.springframework.context.annotation.ComponentScan;

/**
 *
 * @author dragank
 */
@Configuration
@EnableTransactionManagement
@ComponentScan("rs.gecko.interview.atmsapp")
public class BeansLister {
    
    protected final Logger logr = LoggerFactory.getLogger(this.getClass());
    
    public Map eclipselinkProperties() {
        Map properties = new HashMap();
        // Common, javax
        properties.put(PersistenceUnitProperties.JDBC_URL, "jdbc:h2:mem:test;");
        properties.put(PersistenceUnitProperties.JDBC_DRIVER, "org.h2.Driver");
        properties.put(PersistenceUnitProperties.TRANSACTION_TYPE, PersistenceUnitTransactionType.RESOURCE_LOCAL.name());

        properties.put("exclude-unlisted-classes", "false");
        // Eclipselink
        if(logr.isDebugEnabled()) {
            properties.put(PersistenceUnitProperties.LOGGING_LEVEL, "FINE");
            properties.put(PersistenceUnitProperties.LOGGING_PARAMETERS, "true");
        }
        // TODO dkny - switch here autoupgrade based on what ?
        properties.put(org.eclipse.persistence.config.PersistenceUnitProperties.DDL_GENERATION, "create-or-extend-tables");
        properties.put("eclipselink.connection-pool.default.min", "5");
        properties.put("eclipselink.connection-pool.default.max", "64");
        properties.put("eclipselink.connection-pool.default.wait", "500");
        return properties;
    }

    @Bean
    public PlatformTransactionManager platformTransactionManager(EntityManagerFactory emf) {
        JpaTransactionManager transactionManager
                = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(emf);
        return transactionManager;
    }

//    @Bean
//    public  LocalEntityManagerFactoryBean localEntityManagerFactoryBean() {
//        Map properties = eclipselinkProperties();
//        LocalEntityManagerFactoryBean emfb = new LocalEntityManagerFactoryBean();
//        emfb.setPersistenceUnitName(PersistenceJPAConfig.P_UNIT_VERSIONED_NAME);
//        JpaVendorAdapter vendorAdapter = new EclipseLinkJpaVendorAdapter();
//        emfb.setJpaVendorAdapter(vendorAdapter);
//        emfb.setJpaPropertyMap(properties);
//        return emfb;
//    }

}
