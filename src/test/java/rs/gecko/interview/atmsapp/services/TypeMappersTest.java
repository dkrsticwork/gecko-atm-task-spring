package rs.gecko.interview.atmsapp.services;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import rs.gecko.interview.atmsapp.BeansLister;
import rs.gecko.interview.atmsapp.TestUtils;
import rs.gecko.interview.atmsapp.dto.ATMDTO;
import rs.gecko.interview.atmsapp.entities.ATM;
import rs.gecko.interview.atmsapp.services.TypeMappers.ATMMapper;

/**
 *
 * @author dragank
 */
@RunWith( SpringJUnit4ClassRunner.class )
@ContextConfiguration(
    loader = AnnotationConfigContextLoader.class,
    classes = {
        BeansLister.class
    }
)
public class TypeMappersTest {
    
    @Autowired
    private ATMMapper aTMMapper;

    @Test
    public void testATMtoATMDTO() {
        // TODO dkny - implement test via custom reflective field comparing ???
        System.out.println("undone ATMtoATMDTO");
        ATM atm = TestUtils.createOneATM();
        ATMDTO atmdto = aTMMapper.ATMtoATMDTO(atm);
        System.out.println("unfinished test ATMtoATMDTO");
//        ass
    }
    
    @Test
    public void testATMDTOtoATM() {
        // TODO dkny - implement test via custom reflective field comparing ???
        System.out.println("undone testATMDTOtoATM");
        ATM atm = TestUtils.createOneATM();
        ATMDTO atmdto = aTMMapper.ATMtoATMDTO(atm);
        ATM atmt = aTMMapper.ATMDTOtoATM(atmdto);
        System.out.println("undone testATMDTOtoATM");
//        ass
    }
    
}
