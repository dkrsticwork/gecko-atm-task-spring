package rs.gecko.interview.atmsapp.services;

import org.junit.Test;
import static org.junit.Assert.*;
import rs.gecko.interview.atmsapp.TestUtils;
import rs.gecko.interview.atmsapp.entities.ATM;

/**
 *
 * @author dragank
 */
public class ATMValidationsTest {
    
    public ATMValidationsTest() {
    }

    @Test
    public void testValidate() {
        System.out.println("validate ATM");
        ATM toCheck = TestUtils.createOneATM();
        ATMValidations instance = new ATMValidations();
        ATMValidations.ATMValidationMsg expResult = null;
        ATMValidations.ATMValidationMsg result = instance.validate(toCheck);
        assertEquals(ATMValidations.ATMValidationMsg.OK, result);
        
        toCheck = TestUtils.createOneATM();
        toCheck.setAddress(null);
        result = instance.validate(toCheck);
        assertEquals(ATMValidations.ATMValidationMsg.NULL_ADDRESS, result);
        
        toCheck = TestUtils.createOneATM();
        toCheck.getAddress().setCity(null);
        result = instance.validate(toCheck);
        assertEquals(ATMValidations.ATMValidationMsg.NO_CITY, result);
        toCheck.getAddress().setCity("");
        result = instance.validate(toCheck);
        assertEquals(ATMValidations.ATMValidationMsg.NO_CITY, result);
     
        result = instance.validate(null);
        assertEquals(ATMValidations.ATMValidationMsg.NULL_ATM, result);
        
        toCheck = TestUtils.createOneATM();
        toCheck.getAddress().setGeoLocation(null);
        result = instance.validate(toCheck);
        assertEquals(ATMValidations.ATMValidationMsg.NULL_GEO_LOCATION, result);
        
        toCheck = TestUtils.createOneATM();
        toCheck.setType(null);
        result = instance.validate(toCheck);
        assertEquals(ATMValidations.ATMValidationMsg.NULL_TYPE, result);
    }
    
}
