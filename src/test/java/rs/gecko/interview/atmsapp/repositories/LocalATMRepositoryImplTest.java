package rs.gecko.interview.atmsapp.repositories;

import rs.gecko.interview.atmsapp.BeansLister;
import java.util.List;
import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import rs.gecko.interview.atmsapp.TestUtils;
import rs.gecko.interview.atmsapp.entities.ATM;
import rs.gecko.interview.atmsapp.services.ATMValidations.ATMValidationMsg;
import rs.gecko.interview.atmsapp.services.ATMsService;
import rs.gecko.interview.atmsapp.services.AbstractATMsAppService.ATMServiceMsg;
import rs.gecko.interview.atmsapp.services.TranslatableEnumerator;

/**
 *
 * @author dragank
 */
@RunWith( SpringJUnit4ClassRunner.class )
@ContextConfiguration(
    loader = AnnotationConfigContextLoader.class,
    classes = {
        BeansLister.class
    }
)
public class LocalATMRepositoryImplTest {

    @Autowired
    private ATMsService aTMsService;
    
    /**
     * Test of create method, of class LocalATMRepositoryImpl.
     */
    @Test
    public void testCreateATM() {
        System.out.println("testCreateATM");
        ATM atm = TestUtils.createOneATM();
        ATMServiceMsg opResult = aTMsService.createATM(atm);
        assertEquals(opResult, ATMServiceMsg.SUCCESS);
        assertNotNull(atm.getId());
    }

    /**
     * Test of update method, of class LocalATMRepositoryImpl.
     */
    @Test
    public void testUpdateATM() {
        System.out.println("testUpdateATM");
        ATM atm = TestUtils.createOneATM();
        atm.setDistance(5);
        ATMServiceMsg createResult = aTMsService.createATM(atm);
            ATM expResult = aTMsService.findById(atm.getId());
            expResult.setDistance(0);
            TranslatableEnumerator updateResult = aTMsService.updateATM(expResult);
            if(updateResult == ATMValidationMsg.OK) {
                expResult = aTMsService.findById(atm.getId());
                assertTrue(atm.getDistance() > 0 && 0 == expResult.getDistance());
            }
    }

    /**
     * Test of delete method, of class LocalATMRepositoryImpl.
     */
    @Test
    public void testDeleteATM() {
        System.out.println("testDeleteATM");
        ATM atm = TestUtils.createOneATM();
        aTMsService.createATM(atm);
        aTMsService.delete(atm);
        atm = aTMsService.findById(atm.getId());
        assertNull(atm);
    }

    @Test
    public void testDeleteAllATMs() {
        System.out.println("testDeleteAllATMs");
        ATM atm = TestUtils.createOneATM();
        aTMsService.createATM(atm);
        atm = TestUtils.createOneATM();
        aTMsService.createATM(atm);
        atm = TestUtils.createOneATM();
        aTMsService.createATM(atm);
        atm = TestUtils.createOneATM();
        aTMsService.createATM(atm);
        long oneId = atm.getId();
        aTMsService.deleteAllATMs();
        atm = aTMsService.findById(oneId);
        assertNull(atm);
    }

    @Test
    public void testFindById() {
        System.out.println("testFindById");
        ATM atm = TestUtils.createOneATM();
        aTMsService.createATM(atm);
        ATM result = aTMsService.findById(atm.getId());
        assertNotNull(result);
    }

    @Test
    public void testSelectByCity() {
        System.out.println("testSelectByCity");
        String cityName = "ha";
        ATM atm = TestUtils.createOneATM();
        aTMsService.createATM(atm);
        atm = TestUtils.createOneATM();
        aTMsService.createATM(atm);
        atm = TestUtils.createOneATM();
        aTMsService.createATM(atm);
        atm = TestUtils.createOneATM();
        aTMsService.createATM(atm);
        final List<ATM> atmsByCity = aTMsService.selectByCity(cityName);
        assertTrue(atmsByCity.size() > 0);
    }

}
