package rs.gecko.interview.atmsapp.repositories;

import rs.gecko.interview.atmsapp.BeansLister;
import java.util.Iterator;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import rs.gecko.interview.atmsapp.entities.ATM;

/**
 *
 * @author dragank
 */
@RunWith( SpringJUnit4ClassRunner.class )
@ContextConfiguration(
    loader = AnnotationConfigContextLoader.class,
    classes = {
        BeansLister.class
    }
)
public class RemoteATMRepositoryTest {

    @Autowired
    private RemoteATMRepository remoteATMRepository;
    
    @Test
    public void testLoadATMIterator() {
        System.out.println("testLoadATMIterator");
        Iterator<ATM> atmIterator = remoteATMRepository.loadATMIterator();
        int atmCount = 0;
        while(atmIterator.hasNext()) {
            atmCount++;
            atmIterator.next();
        }
        assert(atmCount > 0);
        System.out.println(String.format("testLoadATMIterator loaded %d ATMs", atmCount));
    }
    
}
