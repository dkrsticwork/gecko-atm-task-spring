var appRoot = location.protocol + '://' + location.host + ":" + '/atmsapp-1.0-SNAPSHOT';
var atmProcessResourcePath = '/resources/v1/atm';
var atmResourceRoot = appRoot + atmProcessResourcePath;

var language = 'sr_RS';

function deleteATM(id) {
    var deletingOneUrl = atmResourceRoot +  '/' + id;
    $.ajax(
        {
            url: 'http://localhost:8084/atmsapp-1.0-SNAPSHOT/resources/v1/atm' +  '/' + id, 
            method: 'DELETE',
            headers: {
                'Accept-Language': language
            },
            contentType: 'application/json', 
            success: function(result) {
                alert(result);
            }
        }
    );
}

function confirmATMDelete(question, cancel, id) {
    var answ = confirm(question);
    if(answ == true)
        deleteATM(id);
    else
        alert(cancel);
}
