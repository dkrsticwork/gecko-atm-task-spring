var appRoot = location.protocol + '://' + location.host + ":" + '/atmsapp-1.0-SNAPSHOT';
var atmProcessResourcePath = '/resources/v1/atmprocesses';
var atmResourceRoot = appRoot + atmProcessResourcePath;

var language = 'sr_RS';

function loadATMs() {
    var loadingURL = atmResourceRoot + '/init';
    $.ajax(
        {
            url: 'http://localhost:8084/atmsapp-1.0-SNAPSHOT/resources/v1/atmprocesses/init', 
            method: 'POST',
            headers: {
                'Accept-Language': language
            },
            contentType: 'application/json', 
            success: function(result) { 
                alert(result); 
            }
        }
    );
}

function clearATMs() {
    var deletingAllUrl = atmResourceRoot + '/all';
    $.ajax(
        {
            url: 'http://localhost:8084/atmsapp-1.0-SNAPSHOT/resources/v1/atmprocesses/all',
            method: 'DELETE',
            headers: {
                'Accept-Language': language
            },
            contentType: 'application/json', 
            success: function(result) {
                alert(result);
            }
        }
    );
}

function addCommonHeader() {
    
}

$(document).ready(function() {
    $("#locales").change(function () {
        var selectedOption = $('#locales').val();
        if (selectedOption != ''){
            language = selectedOption;
        }
    });
});
