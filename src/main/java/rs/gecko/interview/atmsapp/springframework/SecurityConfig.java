package rs.gecko.interview.atmsapp.springframework;

import javax.sql.DataSource;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

/**
 *
 * @author dragank
 */
@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
            .formLogin()
            .loginPage("/login")
            .and()
            .logout()
            .logoutSuccessUrl("/")
            .logoutUrl("/signout")
            .and()
            .httpBasic()
            .realmName("ATM")
            .and()
            .rememberMe()
            .tokenValiditySeconds(2419200)
            .key("spittrKey")
            .and()
            .authorizeRequests()
            .antMatchers("/resources/v1").hasRole("ADMIN")
            .antMatchers(HttpMethod.POST, "/atms/edit").hasRole("ADMIN")
            .anyRequest().permitAll()
            .and()
            .requiresChannel()
            .antMatchers("/login")
            .requiresSecure()
        ;
    }
    
//    // @EnableWebSecurity //
//    @Override
//    protected void configure(HttpSecurity http) throws Exception {
//        http
//            .authorizeRequests()
//            .anyRequest().authenticated()
//            .and()
//            .formLogin()
//            .and()
//            .httpBasic()
//        ;
//    }
//
//    @Override
//    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//        auth
//            .inMemoryAuthentication()
//            .withUser("test").password("test").roles("TEST").and()
//            .withUser("admin").password("password").roles("TEST", "ADMIN")
//        ;
//    }

// TODO dkny - see how this fits into model ?
    /*
    
    public static final String DEF_USERS_BY_USERNAME_QUERY =
    "select username,password,enabled " +
    "from users " +
    "where username = ?";
    public static final String DEF_AUTHORITIES_BY_USERNAME_QUERY =
    "select username,authority " +
    "from authorities " +
    "where username = ?";
    public static final String DEF_GROUP_AUTHORITIES_BY_USERNAME_QUERY =
    "select g.id, g.group_name, ga.authority " +
    "from groups g, group_members gm, group_authorities ga " +
    "where gm.username = ? " +
    "and g.id = ga.group_id " +
    "and g.id = gm.group_id";
    
    */
        
    
    // @Autowired
    private DataSource dataSource;

//    @Override
//    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//        auth
//                .jdbcAuthentication()
//                .dataSource(dataSource)
//                .usersByUsernameQuery(
//                        "select username, password, true "
//                        + "from Spitter where username=?")
//                .authoritiesByUsernameQuery(
//                        "select username, 'ROLE_USER' from ATM where username=?")
//                .passwordEncoder(new StandardPasswordEncoder("53cr3t"));;
//    }
    
//    @Override
//    protected void configure(AuthenticationManagerBuilder auth)
//            throws Exception {
//        auth
//            .ldapAuthentication()
//            .userSearchBase("ou=people")
//            .userSearchFilter("(uid={0})")
//            .groupSearchBase("ou=groups")
//            .groupSearchFilter("member={0}")
//            .passwordCompare()
//            .passwordEncoder(new Md5PasswordEncoder())
//            .passwordAttribute("passcode");
//        
//            .ldapAuthentication()
//            .userSearchBase("ou=people")
//            .userSearchFilter("(uid={0})")
//            .groupSearchBase("ou=groups")
//            .groupSearchFilter("member={0}")
//            .contextSource()
//            // .root("dc=habuma,dc=com");
//            // .ldif("classpath:users.ldif");
//            .url("ldap://habuma.com:389/dc=habuma,dc=com");
//    }
    
}
