package rs.gecko.interview.atmsapp.springframework;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.WebApplicationInitializer;

/**
 *
 * @author dragank
 */
@Configuration
public class AppWebApplicationInitializer implements WebApplicationInitializer {

    private Logger logr = LoggerFactory.getLogger(this.getClass());
    
    @Override
    public void onStartup(ServletContext servletContext) throws ServletException {
        // TODO dkny - load settings here from THE environment variables
        // servletContext.setInitParameter("spring.profiles.active", "dev");
        //servletContext.setInitParameter("logging.level.org.springframework.web", "DEBUG");
    }

}
