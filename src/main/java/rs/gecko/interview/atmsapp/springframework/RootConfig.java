package rs.gecko.interview.atmsapp.springframework;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author dragank
 */
@Configuration
@ComponentScan(
    basePackages = {
        "rs.gecko.interview.atmsapp.repositories",
        "rs.gecko.interview.atmsapp.services"
    }
)
public class RootConfig {
    
}
