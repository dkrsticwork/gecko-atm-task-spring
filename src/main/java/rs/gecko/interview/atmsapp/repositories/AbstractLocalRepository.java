package rs.gecko.interview.atmsapp.repositories;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author dragank
 */
public abstract class AbstractLocalRepository<T> {

    protected final Logger logr = LoggerFactory.getLogger(this.getClass());

    @PersistenceContext
    protected EntityManager em;

}
