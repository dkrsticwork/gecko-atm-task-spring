package rs.gecko.interview.atmsapp.repositories;

import org.springframework.data.repository.CrudRepository;
import rs.gecko.interview.atmsapp.entities.AppUser;

/**
 *
 * @author dragank
 */
public interface ILocalUserRepository extends CrudRepository<AppUser, Long> {
    
    public AppUser findByUsername(String username);
    public AppUser findByUsernameAndPassword(String username, String password);
    
}
