package rs.gecko.interview.atmsapp.repositories;

import java.util.Optional;
import org.springframework.stereotype.Repository;
import rs.gecko.interview.atmsapp.entities.AppUser;
import rs.gecko.interview.atmsapp.services.CommonAlgorithmsImpl;

/**
 *
 * @author dragank
 */
@Repository
public class LocalUserRepository extends AbstractLocalRepository<AppUser> implements ILocalUserRepository {

    @Override
    public AppUser findByUsername(String username) {
        return (AppUser) em
                .createQuery("select au from AppUser au "
                        + " left join fetch au.priviledges aup "
                        + " where username=:username")
                .setParameter("username", username)
                .getSingleResult()
            ;
    }
    
    @Override
    public AppUser findByUsernameAndPassword(String username, String password) {
        password = new CommonAlgorithmsImpl().encrypt(password);
        return (AppUser) em
                .createQuery("select au from AppUser au "
                        + " left join fetch au.priviledges aup "
                        + " where username=:username and password=:password")
                .setParameter("username", username)
                .setParameter("password", password)
                .getSingleResult()
            ;
    }

    @Override
    public <S extends AppUser> S save(S s) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public <S extends AppUser> Iterable<S> saveAll(Iterable<S> itrbl) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Optional<AppUser> findById(Long id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean existsById(Long id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Iterable<AppUser> findAll() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Iterable<AppUser> findAllById(Iterable<Long> itrbl) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public long count() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void deleteById(Long id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete(AppUser t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void deleteAll(Iterable<? extends AppUser> itrbl) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void deleteAll() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
