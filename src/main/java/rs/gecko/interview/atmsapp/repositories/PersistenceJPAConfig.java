package rs.gecko.interview.atmsapp.repositories;

import java.io.InputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.instrument.classloading.InstrumentationLoadTimeWeaver;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.LocalEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import rs.gecko.interview.atmsapp.entities.DomainObject;

import javax.persistence.EntityManagerFactory;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import javax.persistence.spi.PersistenceUnitTransactionType;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author dragank
 */
@Configuration
@EnableTransactionManagement
public class PersistenceJPAConfig implements DisposableBean {

    private Environment environment;
    @Autowired
    public void setEnvironment(Environment environment) {
        this.environment = environment;
    }
    
    // TODO dkny - get rid of versioned name
    public static String P_UNIT_VERSIONED_NAME ;

    private static final String PERSISTENCE_XML_PATH = "META-INF/persistence.xml";
    private static final String PERSISTENCE_TAG_NAME = "persistence-unit";

    private static final transient Logger LOGR = LoggerFactory.getLogger(PersistenceJPAConfig.class);
    private final transient Logger logr = LoggerFactory.getLogger(PersistenceJPAConfig.class);
    
    static {
        P_UNIT_VERSIONED_NAME = findPersistenceUnitNameFromPersistenceXML();
    }
    
    private static String findPersistenceUnitNameFromPersistenceXML() {
        // For most recent - local development purpose.
        InputStream is = ClassLoader.getSystemResourceAsStream(PERSISTENCE_XML_PATH);
        if(is == null)
            is = PersistenceJPAConfig.class.getResourceAsStream("/" + PERSISTENCE_XML_PATH);
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.parse(is);
            Element element = doc.getDocumentElement();
            NodeList nodes = element.getChildNodes();
            for (int i = 0; i < nodes.getLength(); i++) {
                Node node = nodes.item(i);
                String nodeName = node.getNodeName();
                if(node.getAttributes() == null || node.getAttributes().getNamedItem("name") == null)
                    continue;
                String nameAttributeValue = node.getAttributes().getNamedItem("name").getNodeValue();
                if(PERSISTENCE_TAG_NAME.equals(nodeName))
                    return nameAttributeValue;
            }
        } catch (Throwable t) {
            LOGR.warn("In getting " + PERSISTENCE_TAG_NAME + " name from " + PERSISTENCE_XML_PATH + " ... " + t.getMessage());
            LOGR.debug("In getting " + PERSISTENCE_TAG_NAME + " name from " + PERSISTENCE_XML_PATH + " ... " + t);
        }
        LOGR.warn("Returning null name for PU... App;ication is failing to start...");
        return null;
    }

    public static Map hibernateProperties() {
        Map properties = new HashMap();
        // TODO dkny - Wire here JNDI datasource.
        // properties.put("jta-data-source", "jdbc/atmsDemoAppDS");
        // Common, javax
        properties.put("javax.persistence.transactionType", PersistenceUnitTransactionType.RESOURCE_LOCAL);
        properties.put("javax.persistence.jdbc.url", "jdbc:h2:mem:test;");
        properties.put("javax.persistence.jdbc.driver", "org.h2.Driver");
        // Hibernate
        if(LOGR.isDebugEnabled()) {
            properties.put("hibernate.show_sql", "true");
            properties.put("hibernate.format_sql", "true");
            properties.put("hibernate.use_sql_comments", "true");
            properties.put("hibernate.generate_statistics", "true");
        }
        else {
            properties.put("hibernate.show_sql", "false");
            properties.put("hibernate.format_sql", "false");
            properties.put("hibernate.use_sql_comments", "false");
            properties.put("hibernate.generate_statistics", "false");
        }
        properties.put("hibernate.dialect", "org.hibernate.dialect.H2Dialect");
        properties.put("hibernate.hbm2ddl.auto", "update");
        properties.put("hibernate.ejb.naming_strategy", "org.hibernate.cfg.DefaultComponentSafeNamingStrategy");

        properties.put("hibernate.c3p0.min_size", "5");
        properties.put("hibernate.c3p0.max_size", "255");
        properties.put("hibernate.c3p0.timeout", "500");
        properties.put("hibernate.c3p0.max_statements", "500");
        properties.put("hibernate.c3p0.idle_test_period", "2000");
        return properties;
    }
    
    @Bean // Container managed EntityManager
    public LocalContainerEntityManagerFactoryBean localContainerEntityManagerFactoryBean() {
        // TODO dkny - switch here according to profile
        return hibernateCMPEmf();
    }

    // Bean managed EntityManager
    public LocalEntityManagerFactoryBean localEntityManagerFactory() {
        // TODO dkny - switch here according to profile
        return hibernateEmf();
    }

    @Bean
    public PlatformTransactionManager platformTransactionManager(EntityManagerFactory emf) {
        JpaTransactionManager transactionManager
                = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(emf);
        return transactionManager;
    }

    private LocalContainerEntityManagerFactoryBean hibernateCMPEmf() {
        Map properties = hibernateProperties();
        LocalContainerEntityManagerFactoryBean emfb = new LocalContainerEntityManagerFactoryBean();
        emfb.setPersistenceUnitName(P_UNIT_VERSIONED_NAME);
        emfb.setPackagesToScan(new String[] {
            DomainObject.class.getPackage().getName()
        });
        JpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        emfb.setJpaVendorAdapter(vendorAdapter);
        emfb.setJpaPropertyMap(properties);
        emfb.setLoadTimeWeaver(new InstrumentationLoadTimeWeaver());
        return emfb;
    }

    private LocalEntityManagerFactoryBean hibernateEmf() {
        Map properties = hibernateProperties();
        LocalEntityManagerFactoryBean emfb = new LocalEntityManagerFactoryBean();
        emfb.setPersistenceUnitName(P_UNIT_VERSIONED_NAME);
        JpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        emfb.setJpaVendorAdapter(vendorAdapter);
        emfb.setJpaPropertyMap(properties);
        return emfb;
    }

    private void deregisterDrivers() {
        Enumeration<Driver> drivers = DriverManager.getDrivers();
        logr.info("AppWebApplicationInitializer @PreDestroy deregisterDrivers() started...");
        while (drivers.hasMoreElements()) {
            Driver driver = drivers.nextElement();
            try {
                DriverManager.deregisterDriver(driver);
                logr.info("AppWebApplicationInitializer @PreDestroy deregistered " + driver.toString());
            } catch (SQLException sqlex) {
                logr.warn("In AppWebApplicationInitializer @PreDestroy deregisterDrivers()", sqlex);
            }
        }
        logr.info("AppWebApplicationInitializer @PreDestroy deregisterDrivers() finished.");
    }

    @Override
    public void destroy() throws Exception {
        deregisterDrivers();
    }

}
