package rs.gecko.interview.atmsapp.repositories;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.InputStream;
import java.io.Serializable;
import java.net.URL;
import java.util.Iterator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import rs.gecko.interview.atmsapp.entities.ATM;

/**
 *
 * @author dragank
 */
@Repository
public class RemoteATMRepository implements Serializable {

    private static final String ING_NL_API_LOCATOR_ATMS = "https://www.ing.nl/api/locator/atms/";

    private static final int DIRTY_CHARACTERS_NUMBER = 5;

    private Logger logr = LoggerFactory.getLogger(this.getClass());

    /**
     * From https://www.ing.nl/api/locator/atms/
     * prepare iterator for lazy from input stream loading.
     * Before returning iterator reads : DIRTY_CHARACTERS_NUMBER
     * characters from beginning and voids them.
     * @return null on error of any kind
    */
    public Iterator<ATM> loadATMIterator() {
        logr.debug("RemoteATMRepository loadATMIterator() started...");
        Iterator<ATM> atmIterator = null;
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            URL ingURL = new URL(ING_NL_API_LOCATOR_ATMS);
            InputStream bodyStream = ingURL.openStream();
            
            // Empty several "dirty" characters from beginning.
            for (int i = 0; i < DIRTY_CHARACTERS_NUMBER; i++) {
                char dirtyChar = (char) bodyStream.read();
                logr.trace("Read " + dirtyChar + " as dirtyChar");
            }

            atmIterator = objectMapper.readerFor(ATM.class)
                            .readValues(bodyStream);

            logr.debug("RemoteATMRepository loadATMIterator() is now returning.");
            return atmIterator;
        } catch (Exception e) {
            logr.error("In loadFromING()", e);
            logr.debug("RemoteATMRepository loadATMIterator() is returning null. ");
            return null;
        }
    }

}
