package rs.gecko.interview.atmsapp.repositories;

import java.io.Serializable;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import rs.gecko.interview.atmsapp.entities.ATM;

/**
 * @author dragank
 */
@Repository
public class LocalATMRepository extends AbstractLocalRepository<ATM> implements ILocalATMRepository, Serializable {

    private static final long serialVersionUID = -4573922566301070354L;

    @Override
    public ATM save(ATM atm) {
        logr.trace(String.format("Saving ATM %s.", atm.toJSON()));
        if (atm.getId() == null)
            em.persist(atm);
        else
            atm = em.merge(atm);
        return atm;
    }

    @Override
    public <S extends ATM> Iterable<S> saveAll(Iterable<S> itrbl) {
        logr.trace(String.format("Saving ATMs..."));
        if (itrbl != null) {
            Iterator si = itrbl.iterator();
            while (si.hasNext())
                save((ATM) si.next());
        }
        return itrbl;
    }

    @Override
    public Optional<ATM> findById(Long id) {
        return Optional.ofNullable(em.find(ATM.class, id));
    }

    @Override
    public Iterable<ATM> findAllById(Iterable<Long> ids) {
        Query q = em.createQuery(
                "select a from ATM a where a.id in :pIds"
        )
                .setParameter("pIds", ids);
        return q.getResultList();
    }

    @Override
    public Iterable<ATM> findAll() {
        Query q = em.createQuery(
                "select a from ATM a where a.id in :pIds"
        );
        return q.getResultList();
    }

    @Override
    public boolean existsById(Long id) {
        return em.find(ATM.class, id) != null;
    }

    @Override
    public void deleteById(Long id) {
        logr.trace(String.format("Deleting ATM %d.", id));
        delete(em.find(ATM.class, id));
    }

    @Override
    public void delete(ATM atm) {
        logr.trace(String.format("Deleting ATM %s.", atm.toJSON()));
        atm = em.find(ATM.class, atm.getId());
        em.remove(atm);
    }

    @Override
    public void deleteAll(Iterable listToDelete) {
        logr.trace("Deleting ATMs from list ...");
        em.createQuery("delete from ATM a where a in :pListToDelete")
                .setParameter("pListToDelete", listToDelete)
                .executeUpdate(); // TODO dkny - How to filter by creator user or role?
    }

    @Override
    public void deleteAll() {
        logr.trace("Deleting all ATMs.");
        em.createQuery("delete from ATM a")
                .executeUpdate(); // TODO dkny - How to filter by creator user or role?
    }

    @Override
    public long count() {
        Query q = em.createQuery(
                "select count(a) from ATM a"
        );
        return (Long) q.getSingleResult();

    }

    /**
     * Paged city names search.
     *
     * @param cityName for search to begin with
     * @param paging   as two member int array;<br/>
     *                 first result position as first element;<br/>
     *                 page size as second element;<br/>
     * @return city names list paged for not null paging parameters
     */
    @Override
    public List<String> selectCityNames(String cityName, int... paging) {
        if (cityName == null || cityName.isEmpty() || cityName.replace("%", "").isEmpty()) {
            return Collections.EMPTY_LIST;
        }
        Query q = em.createQuery(
                "select distinct a.address.city from ATM a where upper(a.address.city) like :pCityName order by a.address.city asc"
        )
                .setParameter("pCityName", cityName.toUpperCase() + "%");
        if (paging != null && paging.length == 2 && paging[0] >= 0 && paging[1] >= 0) {
            q.setFirstResult(paging[0]);
            q.setMaxResults(paging[1]);
        }
        return q.getResultList();
    }

    /**
     * Paged ATMs by city name search.
     *
     * @param cityName for search to begin with
     * @param paging   as two member int array;<br/>
     *                 first result position as first element;<br/>
     *                 page size as second element;<br/>
     * @return ATMs list paged for not null paging parameters
     */
    @Override
    public List<ATM> selectByCity(String cityName, int... paging) {
        if (cityName == null || cityName.isEmpty() || cityName.replace("%", "").isEmpty()) {
            return Collections.EMPTY_LIST;
        }
        Query q = em.createQuery(
                "select a from ATM a where upper(a.address.city) like :pCityName order by a.address.city asc"
        )
                .setParameter("pCityName", cityName.toUpperCase() + "%");
        if (paging != null && paging.length == 2 && paging[0] >= 0 && paging[1] >= 0) {
            q.setFirstResult(paging[0]);
            q.setMaxResults(paging[1]);
        }
        return q.getResultList();
    }

    /**
     * ATMs by city name counter.
     *
     * @param cityName to find count of ATMs
     * @return count of cities for name set
     */
    @Override
    public long countByAddressCity(String cityName) {
        if (cityName == null || cityName.isEmpty() || cityName.replace("%", "").isEmpty()) {
            return 0;
        }
        Query q = em.createQuery(
                "select count(a) from ATM a where upper(a.address.city) like :pCityName"
        )
                .setParameter("pCityName", cityName.toUpperCase() + "%");
        return (Long) q.getSingleResult();
    }

}
