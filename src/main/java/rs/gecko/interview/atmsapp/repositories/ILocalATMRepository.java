package rs.gecko.interview.atmsapp.repositories;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import rs.gecko.interview.atmsapp.entities.ATM;

/**
 * @author dragank
 */
public interface ILocalATMRepository extends CrudRepository<ATM, Long> {

    List<String> selectCityNames(String cityName, int ... paging);
    List<ATM> selectByCity(String cityName, int ... paging);
    long countByAddressCity(String cityName);
    
}