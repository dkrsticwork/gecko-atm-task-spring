package rs.gecko.interview.atmsapp.rests;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestParam;

import rs.gecko.interview.atmsapp.entities.ATM;
import rs.gecko.interview.atmsapp.services.IATMsService;
import rs.gecko.interview.atmsapp.services.TranslatableEnumerator;

/**
 * This API operates on entity level.
 * @author dragank
 */
@RestController
@RequestMapping("/resources/v1/atm")
public class ATMsRest implements IATMsRest {
    
    private IATMsService iATMsService;
@Autowired
    public void setiATMsService(IATMsService iATMsService) {
        this.iATMsService = iATMsService;
    }
    
    
    @PostMapping(value = "/")
    @Override
    public TranslatableEnumerator post(@RequestBody ATM input) {
        return iATMsService.createATM(input);
    }
    
    @GetMapping(value = "/{id}")
    @Override
    public ATM get(@PathVariable Long id) {
        ATM foundATM = iATMsService.findById(id);
        return foundATM;
    }    
    
    @GetMapping
    @Override
    public List<ATM> listByCityName(@RequestParam String cityName) {
        List<ATM> foundATMs = iATMsService.selectByCity(cityName);
        return foundATMs;
    }
    
    @GetMapping(value = "/count")
    @Override
    public long countByCityName(@RequestParam String cityName) {
        return iATMsService.countbyCity(cityName);
    }
    
    @PutMapping(value = "/")
    @Override
    public TranslatableEnumerator put(@RequestBody ATM input) {
        return iATMsService.updateATM(input);
    }

    @DeleteMapping(value = "/{id}")
    @Override
    public TranslatableEnumerator delete(@PathVariable long id) {
        return iATMsService.delete(iATMsService.findById(id));
    }

    @GetMapping(value = "/cityNames")
    @Override
    public List<String> listCityNames(@RequestParam String cityName) {
        return iATMsService.selectCityNames(cityName);
    }
    
}
