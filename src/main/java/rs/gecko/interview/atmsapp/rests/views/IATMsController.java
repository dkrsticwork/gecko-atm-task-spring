package rs.gecko.interview.atmsapp.rests.views;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;

public interface IATMsController {
    
    String atms();
    String foundCities(String searchString, Model model);
    String foundATMs(String searchString, Model model);
    String getATMs(String searchString, Model model);
    String postATMs(String searchString, Model model);
    String editATM(Long id, Model model);
    String newATM(Model model);
    String editATM(Model model);
    String deleteATM(@PathVariable Long id);
    String handleException(Exception ex);
    
}