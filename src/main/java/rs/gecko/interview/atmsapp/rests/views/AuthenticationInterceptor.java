package rs.gecko.interview.atmsapp.rests.views;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;

import java.io.IOException;

import org.springframework.web.filter.GenericFilterBean;

/**
 *
 * @author dragank
 */
public class AuthenticationInterceptor extends GenericFilterBean {

    @Override
    public void doFilter(
            ServletRequest request,
            ServletResponse response,
            FilterChain chain) throws IOException, ServletException {

        chain.doFilter(request, response);
    }
}
