package rs.gecko.interview.atmsapp.rests.views;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import rs.gecko.interview.atmsapp.dto.ATMDTO;
import rs.gecko.interview.atmsapp.entities.ATM;
import rs.gecko.interview.atmsapp.rests.ATMsRest;
import rs.gecko.interview.atmsapp.services.AbstractATMsAppService.ATMServiceMsg;
import rs.gecko.interview.atmsapp.services.TranslatableEnumerator;
import rs.gecko.interview.atmsapp.services.TypeMappers.ATMMapper;

/**
 * This controller operates on basic dto level.
 * @author dragank
 */
@Controller
public class ATMsController implements IATMsController {

    private Logger logr = LoggerFactory.getLogger(this.getClass());
    
    private ATMsRest aTMsRest;
    @Autowired
    public void setaTMsRest(ATMsRest aTMsRest) {
        this.aTMsRest = aTMsRest;
    }

    private ATMMapper aTMMapper;
    @Autowired
    @Qualifier("aTMMapper")
    public void setaTMMapper(ATMMapper aTMMapper) {
        this.aTMMapper = aTMMapper;
    }
    
    @GetMapping(value = "/atms")
    @Override
    public String atms() {
        return "atms/search";
    }

    @RequestMapping(value = "/atms/cities/search")
    @Override
    public String foundCities(@RequestParam("searchString") String searchString, Model model) {
    	model.addAttribute("searchString", searchString);
        List<String> listCityNames = aTMsRest.listCityNames(searchString);
        model.addAttribute("foundCities", listCityNames);
        return "redirect:/atms/cities/search";
    }

    @RequestMapping(value = "/atms/search")
    @Override
    public String foundATMs(@RequestParam(name = "searchString", required = false) String searchString, Model model) {
        model.addAttribute("searchString", searchString);
        List<ATMDTO> foundATMs = aTMMapper.ATMtoATMDTO(aTMsRest.listByCityName(searchString));
        model.addAttribute("searchResult", foundATMs);
        long countByCityName = aTMsRest.countByCityName(searchString);
        model.addAttribute("count", countByCityName);
        return "redirect:/atms/search";
    }

    @GetMapping(value = "/atms/search")
    @Override
    public String getATMs(@RequestParam(name = "searchString", required = false) String searchString, Model model) {
        model.addAttribute("searchString", searchString);
        List<ATMDTO> foundATMs = aTMMapper.ATMtoATMDTO(aTMsRest.listByCityName(searchString));
        model.addAttribute("searchResult", foundATMs);
        long countByCityName = aTMsRest.countByCityName(searchString);
        model.addAttribute("count", countByCityName);
        return "/atms/search";
    }

    @PostMapping(value = "/atms/search")
    @Override
    public String postATMs(@RequestParam(name = "searchString", required = false) String searchString, Model model) {
        model.addAttribute("searchString", searchString);
        List<ATMDTO> foundATMs = aTMMapper.ATMtoATMDTO(aTMsRest.listByCityName(searchString));
        model.addAttribute("searchResult", foundATMs);
        long countByCityName = aTMsRest.countByCityName(searchString);
        model.addAttribute("count", countByCityName);
        return "redirect:/atms/search";
    }

    @GetMapping(value = "/atms/edit/{id}")
    @Override
    public String editATM(@PathVariable Long id, Model model) {
        ATMDTO atmToEdit = aTMMapper.ATMtoATMDTO(aTMsRest.get(id));
        model.asMap().put("atm", atmToEdit);
        return "redirect:/atms/edit";
    }

    @GetMapping(value = "/atms/edit")
    @Override
    public String newATM(Model model) {
        ATMDTO newATM = aTMMapper.ATMtoATMDTO(new ATM());
        model.asMap().put("atm", newATM);
        return "/atms/edit";
    }

    @PostMapping(value = "/atms/edit")
    @Override
    public String editATM(Model model) {
        ATMDTO atmdto = (ATMDTO)model.asMap().get("atmdto");
        ATM atm = aTMMapper.ATMDTOtoATM(atmdto);
        // First try to do save or update:
        TranslatableEnumerator operationResult = ATMServiceMsg.FAILURE;
        if (atm.getId() == null) {
            operationResult = aTMsRest.post(atm);
        } else {
            operationResult = aTMsRest.put(atm);
        }
        // Adjust navigation according to operation outcome:
        if (operationResult == ATMServiceMsg.SUCCESS) {
            return "redirect:atms/edit";
        } else {
            return "atms/edit";
        }
    }

    @DeleteMapping(value = "/atms/{id}")
    @Override
    public String deleteATM(@PathVariable Long id) {
        aTMsRest.delete(id);
        return "redirect:search";
    }

    // TODO dkny - incorporate exception to response code with message mapping
    // search through api.
    @ExceptionHandler(Exception.class)
    @Override
    public String handleException(Exception ex) {
        logr.error("Single TODO handler caught exception. Redirecting to \"error/exception\"", ex);
        return "error/exception";
    }

}
