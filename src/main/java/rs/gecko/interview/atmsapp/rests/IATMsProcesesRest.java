package rs.gecko.interview.atmsapp.rests;

import rs.gecko.interview.atmsapp.services.TranslatableEnumerator;

/**
 * 
 * @author dragank
 */
public interface IATMsProcesesRest {

    TranslatableEnumerator init();
    TranslatableEnumerator deleteAll();

}