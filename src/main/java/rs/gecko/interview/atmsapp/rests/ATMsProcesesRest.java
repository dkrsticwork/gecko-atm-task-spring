package rs.gecko.interview.atmsapp.rests;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import rs.gecko.interview.atmsapp.services.IATMsService;
import rs.gecko.interview.atmsapp.services.TranslatableEnumerator;

/**
 * This API operates on process level.
 * @author dragank
 */
@RestController
@RequestMapping("/resources/v1/atmprocesses")
public class ATMsProcesesRest implements IATMsProcesesRest {

    private IATMsService iATMsService;
    @Autowired
    public void setiATMsService(IATMsService iATMsService) {
        this.iATMsService = iATMsService;
    }
    
    @PostMapping(value = "/init")
    @Override
    public TranslatableEnumerator init() {
        return iATMsService.init();
    }
    
        
    @DeleteMapping(value = "/all")
    @Override
    public TranslatableEnumerator deleteAll() {
        return iATMsService.deleteAllATMs();
    }
    
}
