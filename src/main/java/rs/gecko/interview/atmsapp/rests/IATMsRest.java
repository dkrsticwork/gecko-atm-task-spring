package rs.gecko.interview.atmsapp.rests;

import java.util.List;
import rs.gecko.interview.atmsapp.entities.ATM;
import rs.gecko.interview.atmsapp.services.TranslatableEnumerator;

/**
 * 
 * @author dragank
 */
public interface IATMsRest {

    TranslatableEnumerator post(ATM input);
    ATM get(Long id);
    List<ATM> listByCityName(String cityName);
    long countByCityName(String cityName);
    TranslatableEnumerator put(ATM input);
    TranslatableEnumerator delete(long id);
    List<String> listCityNames(String cityName); 
    
}