package rs.gecko.interview.atmsapp.services;

import java.io.Serializable;
import java.util.Locale;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import rs.gecko.interview.atmsapp.entities.ATM;
import static rs.gecko.interview.atmsapp.services.ATMValidations.ATMValidationMsg.*;

/**
 *
 * @author dragank
 */
@Service
public class ATMValidations implements Serializable {

    private static final long serialVersionUID = -7693733141964671494L;

    private final Logger logr = LoggerFactory.getLogger(this.getClass());
    
    /**
     * Enumeration of validation messages.
     */
    public static enum ATMValidationMsg implements TranslatableEnumerator {
        /**
         * Validation passed with no errors.
         */OK,
        /**
         * Null received for validation.
         */NULL_ATM,
        /**
         * Null address is set.
         */NULL_ADDRESS,
        /**
         * Null or empty city.
         */NO_CITY,
        /**
         * Null GeoLocation.<br/>
         * We do not check lat/long.
         */NULL_GEO_LOCATION,
         /**
          * Null ATM ATM_TYPES is received.
          */NULL_TYPE,
        /**
         * Unexpected validation error @see logs.
         */
        NOK;

        @Override
        public String getName() {
            return this.name();
        }        
        
        @Override
        public String readTranslated(Locale... locale) {
            // TODO dkny translate using Thymeleaf bundler
            return name();
        }

    }

    /**
     * Simple ATM null or empty fields validation.
     * @param ATM toCheck
     * @return ATMValidationMsg for detected error.
     */
    public ATMValidationMsg validate(ATM toCheck) {
        ATMValidationMsg validationResult = NOK;
        try {
            if(toCheck == null)
                validationResult = NULL_ATM;
            else if(toCheck.getAddress() == null)
                validationResult = NULL_ADDRESS;
            else if(toCheck.getAddress().getCity() == null 
                    || 
                    toCheck.getAddress().getCity().isEmpty()
                    )
                validationResult = NO_CITY;
            else if(toCheck.getAddress().getGeoLocation() == null)
                validationResult = NULL_GEO_LOCATION;
            else if(toCheck.getType() == null)
                validationResult = NULL_TYPE;
            else
                validationResult = OK;
        }
        catch(Exception e) {
            logr.error("In validate atm:" + CommonDumper.DOMAIN_LOG_DUMP_FORMAT, toCheck.toJSON(), e);
        }
        return validationResult;
    }
    
}
