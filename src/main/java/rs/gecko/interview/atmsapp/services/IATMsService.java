package rs.gecko.interview.atmsapp.services;

import java.util.List;
import rs.gecko.interview.atmsapp.entities.ATM;
import rs.gecko.interview.atmsapp.services.AbstractATMsAppService.ATMServiceMsg;

/**
 * 
 * @author dragank
 */
public interface IATMsService {

    ATMServiceMsg createATM(ATM input);
    ATM findById(Long id);
    List<ATM> selectByCity(String cityName);
    long countbyCity(String cityName);
    ATMServiceMsg updateATM(ATM input);
    ATMServiceMsg delete(ATM toDelete);
    ATMServiceMsg deleteAllATMs();
    ATMServiceMsg init();
    List<String> selectCityNames(String cityName);
    
}
