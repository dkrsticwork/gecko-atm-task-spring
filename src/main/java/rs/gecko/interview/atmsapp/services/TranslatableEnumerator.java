package rs.gecko.interview.atmsapp.services;

import java.util.Locale;

/**
 * 
 * @author dragank
 */
public interface TranslatableEnumerator {
    
    public String getName();
    public String readTranslated(Locale ... locale);
    
}
