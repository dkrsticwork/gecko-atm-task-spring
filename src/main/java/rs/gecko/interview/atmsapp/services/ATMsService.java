package rs.gecko.interview.atmsapp.services;

import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import org.springframework.transaction.annotation.Transactional;
import rs.gecko.interview.atmsapp.entities.ATM;
import rs.gecko.interview.atmsapp.repositories.RemoteATMRepository;
import rs.gecko.interview.atmsapp.repositories.ILocalATMRepository;

/**
 *
 * @author dragank
 */
@Service
@Transactional
public class ATMsService extends AbstractATMsAppService implements IATMsService {

    private ILocalATMRepository localATMRepository;
    @Autowired
    public void setLocalATMRepository(ILocalATMRepository localATMRepository) {
        this.localATMRepository = localATMRepository;
    }
    
    private RemoteATMRepository remoteATMRepository;
    @Autowired
    public void setRemoteATMRepository(RemoteATMRepository remoteATMRepository) {
        this.remoteATMRepository = remoteATMRepository;
    }
    
    private ATMValidations atmValidations;
    @Autowired
    public void setAtmValidations(ATMValidations atmValidations) {
        this.atmValidations = atmValidations;
    }
    
    @Override
    public ATMServiceMsg createATM(ATM input) {
        ATMValidations.ATMValidationMsg validationMessage = atmValidations.validate(input);
        if(ATMValidations.ATMValidationMsg.OK == validationMessage) {
            localATMRepository.save(input);
            return ATMServiceMsg.SUCCESS;
        }
        return ATMServiceMsg.FAILURE;
    }
    
    @Override
    public ATM findById(Long id) {
        return localATMRepository.findById(id).orElse(null);
    }

    @Override
    public List<ATM> selectByCity(String cityName) {
        return localATMRepository.selectByCity(cityName);
    }

    @Override
    public long countbyCity(String cityName) {
        return localATMRepository.countByAddressCity(cityName);
    }
    
    @Override
    public ATMServiceMsg updateATM(ATM input) {
        ATMValidations.ATMValidationMsg validationMessage = atmValidations.validate(input);
        if(ATMValidations.ATMValidationMsg.OK == validationMessage) {
            localATMRepository.save(input);
            return ATMServiceMsg.SUCCESS;
        }
        return ATMServiceMsg.FAILURE;
    }

    @Override
    public ATMServiceMsg delete(ATM toDelete) {
        try {
            localATMRepository.delete(toDelete);
            return ATMServiceMsg.SUCCESS;
        }
        catch(Throwable t) {
            logr.error("Error in deleting atm " + (toDelete == null ? toDelete : toDelete.toJSON()), t);
            return ATMServiceMsg.FAILURE;
        }
    }

    @Override
    public ATMServiceMsg deleteAllATMs() {
        try {
            localATMRepository.deleteAll();
            return ATMServiceMsg.SUCCESS;
        }
        catch(Throwable t) {
            logr.error("Error in deleting all atms.", t);
            return ATMServiceMsg.FAILURE;
        } 
    }

    @Override
    public ATMServiceMsg init() {
        ATMServiceMsg progressResult = ATMServiceMsg.FAILURE;
        try {
            progressResult = deleteAllATMs();
            if (progressResult == ATMServiceMsg.SUCCESS) {
                Iterator<ATM> atmIterator = remoteATMRepository.loadATMIterator();
                while (atmIterator.hasNext()) {
                    localATMRepository.save(atmIterator.next());
                }
            }
            progressResult = ATMServiceMsg.SUCCESS;
        }
        catch(Throwable t) {
            logr.error("Atm init failed...", t);
            progressResult = ATMServiceMsg.FAILURE;
        }
        return progressResult;
    }

    @Override
    public List<String> selectCityNames(String cityName) {
        return localATMRepository.selectCityNames(cityName);
    }

}
