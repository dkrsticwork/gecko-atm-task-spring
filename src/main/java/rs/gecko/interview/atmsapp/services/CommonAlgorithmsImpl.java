package rs.gecko.interview.atmsapp.services;

import org.jasypt.util.password.StrongPasswordEncryptor;

/**
 *
 * @author dragank
 */
public class CommonAlgorithmsImpl {

    public String encrypt(String password) {
        return new StrongPasswordEncryptor().encryptPassword(password);
    }
    
}
