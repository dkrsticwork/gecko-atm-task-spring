package rs.gecko.interview.atmsapp.services;

import org.springframework.stereotype.Service;

/**
 *
 * @author dragank
 */
@Service
public class SecurityServiceImpl {

    public String hashPassword(String password) {
        return new CommonAlgorithmsImpl().encrypt(password);
    }
    
}
