package rs.gecko.interview.atmsapp.services;

import java.util.Locale;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author dragank
 */ 
public abstract class AbstractATMsAppService {

    protected final Logger logr = LoggerFactory.getLogger(this.getClass());
    
    public static enum ATMServiceMsg implements TranslatableEnumerator {

        SUCCESS,
        FAILURE;

        @Override
        public String getName() {
            return this.name();
        }
        
        @Override
        public String readTranslated(Locale... locale) {
            // TODO dkny translate using Thymeleaf bundler
            return name();
        }

    }
    
}
