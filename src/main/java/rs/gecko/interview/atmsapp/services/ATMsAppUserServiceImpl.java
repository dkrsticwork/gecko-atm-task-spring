package rs.gecko.interview.atmsapp.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import rs.gecko.interview.atmsapp.dto.AppUserDTO;
import rs.gecko.interview.atmsapp.entities.AppUser;
import rs.gecko.interview.atmsapp.repositories.ILocalUserRepository;

/**
 *
 * @author dragank
 */
@Service
public class ATMsAppUserServiceImpl implements UserDetailsService {

    private ILocalUserRepository localUserRepository;
    @Autowired
    public void setLocalUserRepository(ILocalUserRepository localUserRepository) {
        this.localUserRepository = localUserRepository;
    }
    
    @Override
    public AppUserDTO loadUserByUsername(String username) throws UsernameNotFoundException {
        AppUserDTO foundDetails = new AppUserDTO();
        try {
            AppUser foundUser = localUserRepository.findByUsername(username);
             
            // TODO dkny - add attrubute mapping;
            return foundDetails;
        }
        catch(Exception e) {
            throw new UsernameNotFoundException("In trying to find AppUser by username " + username, e);
        }
    }

}
