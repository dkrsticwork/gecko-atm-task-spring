package rs.gecko.interview.atmsapp.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Serializable;

/**
 *
 * @author dragank
 */
public class CommonDumper implements Serializable {

    public static final String DOMAIN_LOG_DUMP_FORMAT = "#%s:#";
    
    public static void sinkToJSON(OutputStream sink, Object value) {
        new CommonDumper().doSink(sink, value);
    }

    private void doSink(OutputStream sink, Object value) {
        try {
            new ObjectMapper().writeValue(sink, value);
        } catch (IOException ioex) {
            ioex.printStackTrace(System.err);
        }
    }

}
