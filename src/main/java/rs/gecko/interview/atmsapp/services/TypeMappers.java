package rs.gecko.interview.atmsapp.services;

import java.util.ArrayList;
import java.util.List;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import rs.gecko.interview.atmsapp.dto.ATMDTO;
import rs.gecko.interview.atmsapp.entities.ATM;

/**
 *
 * @author dragank
 */
@Service
public class TypeMappers {

    private static final transient Logger logr = LoggerFactory.getLogger(TypeMappers.class);
    
    @Service("aTMMapper")
    public class ATMMapper {

        private ModelMapper mmm = new ModelMapper();

        public ATMDTO ATMtoATMDTO(ATM source) {
            if(source == null)
                return null;
            mmm.getConfiguration().setMatchingStrategy(MatchingStrategies.LOOSE);
            ATMDTO result = mmm.map(source, ATMDTO.class);
            result.setAtmType(source.getType() == null ? "" : source.getType().name());
            return result;
        }

        public ATM ATMDTOtoATM(ATMDTO source) {
            if(source == null)
                return null;
            mmm.getConfiguration().setMatchingStrategy(MatchingStrategies.LOOSE);
            ATM result = mmm.map(source, ATM.class);
            try {
                if(source.getAtmType() != null)
                    result.setType(ATM.ATM_TYPES.valueOf(source.getAtmType()));
            }
            catch(Exception ex) {
                logr.error("In converting " + source.getAtmType() + " to ATM.ATM_TYPES", ex);
            }
            return result;
        }

        public List<ATMDTO> ATMtoATMDTO(List<ATM> source) {
            List<ATMDTO> result = new ArrayList<>();
            for(ATM atm : source)
                result.add(ATMtoATMDTO(atm));
            return result;
        }

        public List<ATM> ATMDTOtoATM(List<ATMDTO> source) {
            List<ATM> result = new ArrayList<>();
            for(ATMDTO atmdto : source)
                result.add(ATMDTOtoATM(atmdto));
            return result;
        }
    }

}
