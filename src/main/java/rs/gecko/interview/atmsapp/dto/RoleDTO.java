package rs.gecko.interview.atmsapp.dto;

import org.springframework.security.core.GrantedAuthority;

/**
 *
 * @author dragank
 */
public class RoleDTO implements GrantedAuthority {

    private String authority;
    
    @Override
    public String getAuthority() {
        return authority;
    }

}
