package rs.gecko.interview.atmsapp.dto;

import javax.validation.constraints.Digits;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

/**
 *
 * @author dragank
 */
public class AddressDTO {

    private String street;
    
    private String housenumber;
    
    private String postalcode;
    
    @NotEmpty
    @Size(min = 1, max = 255)
    private String city;
    
    @Digits(integer = 2, fraction = 5)
    @Min(-90)
    @Max(90)
    private float lat;
    
    @Digits(integer = 3, fraction = 5)
    @Min(-180)
    @Max(180)
    private float lng;

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getHousenumber() {
        return housenumber;
    }

    public void setHousenumber(String housenumber) {
        this.housenumber = housenumber;
    }

    public String getPostalcode() {
        return postalcode;
    }

    public void setPostalcode(String postalcode) {
        this.postalcode = postalcode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public float getLat() {
        return lat;
    }

    public void setLat(float lat) {
        this.lat = lat;
    }

    public float getLng() {
        return lng;
    }

    public void setLng(float lng) {
        this.lng = lng;
    }
    
}
