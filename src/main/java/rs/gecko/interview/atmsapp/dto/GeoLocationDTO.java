package rs.gecko.interview.atmsapp.dto;

import javax.validation.constraints.Digits;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

/**
 *
 * @author dragank
 */
public class GeoLocationDTO {

    @Digits(integer = 2, fraction = 5)
    @Min(-90)
    @Max(90)
    private float lat;
    
    @Digits(integer = 3, fraction = 5)
    @Min(-180)
    @Max(180)
    private float lng;

    public float getLat() {
        return lat;
    }

    public void setLat(float lat) {
        this.lat = lat;
    }

    public float getLng() {
        return lng;
    }

    public void setLng(float lng) {
        this.lng = lng;
    }
    
}
