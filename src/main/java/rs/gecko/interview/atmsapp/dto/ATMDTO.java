package rs.gecko.interview.atmsapp.dto;

import javax.validation.constraints.Digits;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

/**
 *
 * @author dragank
 */
public class ATMDTO {

    private Long id;

    private Integer version;
    
    private String addressStreet;
    
    private String addressHousenumber;
    
    private String addressPostalcode;
    
    @NotEmpty
    @Size(min = 1, max = 255)
    private String addressCity;

    @Digits(integer = 2, fraction = 5)
    @Min(-90)
    @Max(90)
    private float addressGeoLocationLat;
    
    @Digits(integer = 3, fraction = 5)
    @Min(-180)
    @Max(180)
    private float addressGeoLocationLng;
    
    @Digits(integer = 5, fraction = 4)
    @Min(0)
    @Max(40192)
    private float distance;
    
    @NotEmpty
    @Size(min = 1, max = 50)
    private String atmType;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	public String getAddressStreet() {
        return addressStreet;
    }

    public void setAddressStreet(String addressStreet) {
        this.addressStreet = addressStreet;
    }

    public String getAddressHousenumber() {
        return addressHousenumber;
    }

    public void setAddressHousenumber(String addressHousenumber) {
        this.addressHousenumber = addressHousenumber;
    }

    public String getAddressPostalcode() {
        return addressPostalcode;
    }

    public void setAddressPostalcode(String addressPostalcode) {
        this.addressPostalcode = addressPostalcode;
    }

    public String getAddressCity() {
        return addressCity;
    }

    public void setAddressCity(String addressCity) {
        this.addressCity = addressCity;
    }

    public float getAddressGeoLocationLat() {
        return addressGeoLocationLat;
    }

    public void setAddressGeoLocationLat(float addressGeoLocationLat) {
        this.addressGeoLocationLat = addressGeoLocationLat;
    }

    public float getAddressGeoLocationLng() {
        return addressGeoLocationLng;
    }

    public void setAddressGeoLocationLng(float addressGeoLocationLng) {
        this.addressGeoLocationLng = addressGeoLocationLng;
    }

    public float getDistance() {
        return distance;
    }

    public void setDistance(float distance) {
        this.distance = distance;
    }

    public String getAtmType() {
        return atmType;
    }

    public void setAtmType(String atmType) {
        this.atmType = atmType;
    }

}
