package rs.gecko.interview.atmsapp.entities;

import java.io.ByteArrayOutputStream;
import java.io.Serializable;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import rs.gecko.interview.atmsapp.services.CommonDumper;

/**
 *
 * @author dragank
 */
@Entity
public class ATM extends DomainObject implements Serializable {

    private static final long serialVersionUID = 4335451425703494762L;

    public static enum ATM_TYPES {
        ING
    }

    @Embedded
    private Address address;
    
    private float distance;
    
    @Enumerated(EnumType.STRING)
    private ATM_TYPES type;
    
    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public float getDistance() {
        return distance;
    }

    public void setDistance(float distance) {
        this.distance = distance;
    }

    public ATM_TYPES getType() {
        return type;
    }

    public void setType(ATM_TYPES type) {
        this.type = type;
    }

    /**
     * @return Stringified byte buffer actually. 
     */
    public Object toJSON() {
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        CommonDumper.sinkToJSON(buffer, this);
        return buffer.toString();
    }
    
}
