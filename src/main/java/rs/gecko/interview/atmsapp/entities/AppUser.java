package rs.gecko.interview.atmsapp.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Transient;

/**
 *
 * @author dragank
 */
@Entity
public class AppUser extends DomainObject implements Serializable {

    private String username;
    private String password;
    @Transient
    private String clearPassword;
    
    @ManyToMany
    @JoinTable
    private List<AppPriviledge> priviledges = new ArrayList<AppPriviledge>();

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getClearPassword() {
        return clearPassword;
    }

    public void setClearPassword(String clearPassword) {
        this.clearPassword = clearPassword;
    }

    public List<AppPriviledge> getPriviledges() {
        return priviledges;
    }

    public void setPriviledges(List<AppPriviledge> priviledges) {
        this.priviledges = priviledges;
    }
    
    
    
}
