package rs.gecko.interview.atmsapp.entities;

import java.io.Serializable;
import javax.persistence.Embeddable;


/**
 *
 * @author dragank
 */
@Embeddable
public class GeoLocation implements Serializable {

    private static final long serialVersionUID = -8472774224611734231L;

    private float lat;
    
    private float lng;

    public float getLat() {
        return lat;
    }

    public void setLat(float lat) {
        this.lat = lat;
    }

    public float getLng() {
        return lng;
    }

    public void setLng(float lng) {
        this.lng = lng;
    }

}
