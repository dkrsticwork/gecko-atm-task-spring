package rs.gecko.interview.atmsapp.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

/**
 *
 * @author dragank
 */
@Entity
public class AppRole extends DomainObject implements Serializable {

    private String name;

    @ManyToMany
    @JoinTable
    private List<AppUser> users = new ArrayList<AppUser>();
    
    @ManyToMany
    @JoinTable
    private List<AppPriviledge> priviledges = new ArrayList<AppPriviledge>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<AppUser> getUsers() {
        return users;
    }

    public void setUsers(List<AppUser> users) {
        this.users = users;
    }

    public List<AppPriviledge> getPriviledges() {
        return priviledges;
    }

    public void setPriviledges(List<AppPriviledge> priviledges) {
        this.priviledges = priviledges;
    }
    
}
