package rs.gecko.interview.atmsapp.entities;

import java.io.Serializable;
import javax.persistence.Entity;

/**
 *
 * @author dragank
 */
@Entity
public class AppPriviledge extends DomainObject implements Serializable {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
}
